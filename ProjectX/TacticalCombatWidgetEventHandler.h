#ifndef TACTICAL_COMBAT_WIDGET_EVENT_HANDLER_H
#define TACTICAL_COMBAT_WIDGET_EVENT_HANDLER_H
#include "WidgetEventHandler.h"
#include "Widget.h"
#include "Unit.h"
#include "PlayerManager.h"

class TacticalCombatWidgetEventHandler: public WidgetEventHandler
{
public: 
	TacticalCombatWidgetEventHandler(PlayerManager * pManager, std::vector<Unit*> pUnits) {playerManager = pManager; playerUnits = pUnits;}
	void handleMouseLeftClick(Widget *);

private:
	std::vector<Unit*> playerUnits;
	PlayerManager * playerManager;
};

#endif