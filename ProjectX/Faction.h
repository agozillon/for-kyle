#ifndef FACTION_H
#define FACTION_H
#include <string>
#include "RenderManager.h"


/*
	Faction class, managed by FactionManager class
	the index is directly related to the index in tile
	colour is used to colour in the political map
*/
class Faction
{
public:

	Faction(const std::string &,unsigned int index,const glm::vec4&);
	~Faction();



	const glm::vec4 & getColour() const;
	const unsigned int getIndex() const;
	const std::string &getName() const;

private:
	std::string name;
	unsigned int index;
	glm::vec4 colour;	
	
	Faction();
};

#endif