#ifndef AUDIO_MANAGER_H
#define AUDIO_MANAGER_H
#include <string>
#include <map>
#include "Audio.h"

class AudioManager
{
public:
	static void createInstance();
	static AudioManager * getInstance();
	static void deleteInstance();

	void load(std::string audioName, std::string fileName);
	void playAudio(std::string audioName);
	void resumeAudio(std::string audioName);
	void pauseAudio(std::string audioName);
	void updateVolume(std::string audioName, int volume);

private:
	static AudioManager * instance;
	AudioManager();
	~AudioManager();

	std::map<std::string, Audio*> audioFiles;
};

#endif