#ifndef TACTICAL_MODE_H
#define TACTICAL_MODE_H
#include "LevelManager.h"
#include "EnemyManager.h"
#include "PlayerManager.h"

class TacticalMode
{
public:
	virtual ~TacticalMode(){}

	virtual void draw() = 0;
	virtual void update() = 0;
	virtual void interaction() = 0;

};
#endif