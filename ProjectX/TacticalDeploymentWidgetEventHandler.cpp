#include "TacticalDeploymentWidgetEventHandler.h"
#include "TacticalMapManager.h"

void TacticalDeploymentWidgetEventHandler::handleMouseLeftClick(Widget *widget)
{
	if( widget->getName().compare("AcceptButton") == 0 && widget->getPreviousTimeStamp() + 50 < widget->getTimeStamp() )
	{
		TacticalMapManager::getInstance()->getCombatMode()->setMap(levelManager, *playerUnits, *enemyUnits);
		TacticalMapManager::getInstance()->changeTacticalMode(COMBAT_MODE);
	}
}

void TacticalDeploymentWidgetEventHandler::update()
{

}