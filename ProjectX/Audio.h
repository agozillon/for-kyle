#ifndef AUDIO_H
#define AUDIO_H
#include <string>
#include "bass.h"

class Audio
{
public:
	Audio(const char* fileName);
	void play();
	void resume();
	void pause();
	void updateVolume(int volume);

private:
	HSAMPLE sound;
	HCHANNEL channel;
};

#endif