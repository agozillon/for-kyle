#ifndef BITMAP_LOADER_H
#define BITMAP_LOADER_H
#include "TextureLoader.h"


class BMPLoader: public TextureLoader
{
public:
	BMPLoader();
	Texture load(const std::string filePath);
protected:
	void * invert(void*);
};

#endif