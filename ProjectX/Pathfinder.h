#ifndef PATHFINDER_H
#define PATHFINDER_H
#include <glm/gtc/type_ptr.hpp> // including glm/gtc/type_ptr so that this class can use vec3s 
#include <vector>
#include <cmath>
#include <iostream>
#include <algorithm>
#include "Scenery.h"
#include "Unit.h"

// This PathNode is essentially a node in a calculated path, think of it as a grid point or a node on a grid
// it holds the nodes position and the cost to get to it based off of its F score (G + H). It also stores its 
// previous "Parent" PathNode, essentially the node that was checked before this node and is adjacent to this current node
// this allows us to work backwards and retrace a path really easily!
// admitedly since it's a 2D movement based game the node should really be represented by a 2D vector
// but on the off-chance we repurpose the pathfinder it's useful to keep.
class PathNode
{
public:
	PathNode(float nG, float nH, glm::vec3 pos, PathNode * p) : g(nG), h(nH), position(pos), parent(p){}
	PathNode(){}
	~PathNode(){}
	inline void updateG(float nG){g = nG;}
	inline void updateH(float nH){h = nH;}
	inline void updatePosition(glm::vec3 pos){position = pos;}
	inline void updateParent(PathNode * nParent){parent = nParent;}
	inline float getH(){return h;}
	inline float getG(){return g;}
	inline glm::vec3 getPosition(){return position;}
	inline PathNode * getParent(){return parent;}
	inline float getF(){return h+g;}

	// if the nodes are equal to each other return true else return false
	bool operator==(const PathNode param) const
	{ 
		if((param.position.x >= this->position.x - 0.099f && param.position.x <= this->position.x + 0.099f)
			&& (param.position.y >= this->position.y - 0.099f && param.position.y <= this->position.y + 0.099f ))
		{
			return true;
		}
		else
		{
			return false;
		}
	}



private:
	// balancing the g and h values is required for an optimal A* Pathfinder, too much h = greedy best first search (I.E non-guaranteed shortest path), too much g = Dijkstra's algorithm (guaranted shortest but far less time efficent)
	// perfect balancing of these values = best of both world and thus epic optimal pathing !
	glm::vec3 position;
	float g; // vertex/node values (weight for searching closest to the starting point, when higher than h it'll search much more spread out around the starting node)
	float h; // heruistic (weight for searching closest to the goal, I.E higher it is the more nodes closer to the goal it'll search)

	// links to the node tested before it I.E it's parent in the path chain
	PathNode * parent;
};



// main class that uses PathNode to calculate the best path from an Units starting position to its target position 
class PathFinder
{

public:
	PathFinder() : maxStepCount(1000) {updateStepDistance(0.1f);} // set default step distance
	~PathFinder(){}
	inline void getPath(std::vector<glm::vec3> &nPath){nPath = currentPath; currentPath.clear();}
	inline void setTarget(Unit* currentUnit, glm::vec3 goalPosition){ activeUnit = currentUnit; targetPos = goalPosition;}
	void path(std::vector<Scenery*> obstacles); // function that calculates the path
	void updateStepDistance(float stepDist); // function that sets the distance traversed per step
	inline void updateMaxStepCount(int stepCount){maxStepCount = stepCount;}

private:
	float manhattanHerusticCost(glm::vec3 currentNode, glm::vec3 goalPosition); // herusitic that's calculated as the manhattan distance of the two points
 	float euclidianHerusticCost(glm::vec3 currentNode, glm::vec3 goalPosition); // herustic that's calculated as the euclidian distance of the two points
	void adjacentNodes(std::vector<PathNode*> &oList, std::vector<PathNode*> &cList, PathNode * parent, std::vector<Scenery*> obstacles); // gets all valid adjacent nodes, sorts them and recalculates some of the older ones
	void retracePath(std::vector<glm::vec3> &nPath, PathNode * endNode); // function that retraces the path and gives back the retraced path as a vector of vec3s

	// Open list e.g Nodes not yet checked, nodes only get added to the openlist when we've finished checking an adjacent tile 
	std::vector<PathNode*> openList;
	// Nodes we've already visited to check for the shortest path
	std::vector<PathNode*> closedList; 

	float stepDistance;
	float stepDiagonalCost;
	float stepStraightCost;
	float bias; // bias for node searching in some cases like 0.4f for a step cost the target value may not be in an attainable
	// zone i.e 0.3 so we need to bias it to get within an attainable goal (unfortunately means larger step values means less accuracy)
	int maxStepCount; // value that dictates the maximum number of steps it tries before canceling out! it's up to the user to dictate 
	// a good value for there level.

	glm::vec3 targetPos;
	Unit* activeUnit; 
	std::vector<glm::vec3> currentPath;
};

#endif