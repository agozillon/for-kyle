#ifndef ASSIMP_LOADER_H
#define ASSIMP_LOADER_H
#include "ModelLoader.h"


// simple Assimp model that loads in an Assimp library supported model
// inherits from ModelLoader and passes back the loaded in model as a Mesh
class AssimpLoader: public ModelLoader
{
public:

	AssimpLoader(){}
	~AssimpLoader(){}

	const Renderable * load(const std::string &filePath) const;

private:
	
};

#endif