#include "StrategicMapState.h"
#include "StrategicMapManager.h"
#include "CameraManager.h"


StrategicMapState::StrategicMapState()
{
	StrategicMapManager::createInstance();
}

StrategicMapState::~StrategicMapState()
{
	StrategicMapManager::deleteInstance();
}


void StrategicMapState::draw()
{
	StrategicMapManager::getInstance()->draw();
}


void StrategicMapState::update()
{
	StrategicMapManager::getInstance()->update();
	checkInput();
}

void StrategicMapState::checkInput()
{
	StrategicMapManager::getInstance()->checkKeyPress();
}

void StrategicMapState::onEntry()
{
	CameraManager::getInstance()->setActiveCamera("SM_Default");
}