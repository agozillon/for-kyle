#ifndef TACTICAL_DEPLOYMENT_WIDGET_EVENT_HANDLER_H
#define TACTICAL_DEPLOYMENT_WIDGET_EVENT_HANDLER_H
#include "WidgetEventHandler.h"
#include "Widget.h"
#include "Unit.h"
#include "LevelManager.h"

class TacticalDeploymentWidgetEventHandler: public WidgetEventHandler
{
public: 
	TacticalDeploymentWidgetEventHandler(LevelManager * lManager, std::vector<Unit*> * pUnits, std::vector<Unit*> * eUnits) {levelManager = lManager; playerUnits = pUnits; enemyUnits = eUnits;}
	void handleMouseLeftClick(Widget *);
	void update();

private:
	std::vector<Unit*> * playerUnits;
	std::vector<Unit*> * enemyUnits;
	LevelManager * levelManager;
};

#endif