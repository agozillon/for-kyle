#ifndef TACTICAL_DEPLOYMENT_H
#define TACTICAL_DEPLOYMENT_H
#include "TacticalMode.h"
#include "WidgetManager.h"
#include "BoundingBox.h"

class TacticalDeployment : public TacticalMode
{
public:
	~TacticalDeployment(){}
	TacticalDeployment();
	void draw();
	void update();
	void interaction();
	void initialDeployment(BoundingBox deploymentZone, std::vector<Unit*> units);

private:
	WidgetManager widgetManager;
	LevelManager * levelManager;
	std::vector<Unit*> playerUnits;
	std::vector<Unit*> enemyUnits;
	std::vector<Unit*> playerSelected;
};
#endif