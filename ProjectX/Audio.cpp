#include "Audio.h"
#include <iostream>

Audio::Audio(const char* fileName) 
{
	// loading in the audio and checking if it worked correctly if it didn't then call c++ exit function
	if (sound=BASS_SampleLoad(FALSE, fileName, 0, 0, 3, BASS_SAMPLE_OVER_POS))
	{
		std::cout << "sample " << fileName << " loaded!" << std::endl;
	}
	else
	{
		std::cout << "Can't load sample";
	}

	channel = BASS_SampleGetChannel(sound, FALSE);
}
	
void Audio::play()
{
	// if the channel can't play then output can't play sample
	if(!BASS_ChannelPlay(channel, TRUE))
	{
		std::cout << "Can't play sample" << std::endl;
	}
}
	
void Audio::resume()
{
	// if the channel can't play then output can't play sample
	if(!BASS_ChannelPlay(channel, FALSE))
	{
		std::cout << "Can't play sample" << std::endl;
	}
}

void Audio::pause() 
{
	BASS_ChannelPause(channel);
}

void Audio::updateVolume(int volume)
{
	BASS_SetVolume(volume);
}