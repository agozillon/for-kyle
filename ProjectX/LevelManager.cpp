#include "LevelManager.h"
#include "ForestLevel.h"
#include <iostream>

LevelManager::~LevelManager()
{
	delete levelList["forest"];
}

LevelManager::LevelManager()
{
	//levelList.insert(std::pair<std::string, Level*>("plains", ));
	levelList.insert(std::pair<std::string, Level*>("forest", new ForestLevel()));
	//levelList.insert(std::pair<std::string, Level*>("desert", ));
	currentLevel = levelList["forest"];

}

void LevelManager::init()
{
	currentLevel->init();
}

void LevelManager::draw(glm::mat4 viewProjection)
{
	currentLevel->draw(viewProjection);
}

void LevelManager::switchLevel(std::string levelName)
{
	std::map<std::string, Level*>::const_iterator itr = levelList.find(levelName);

	if( itr == levelList.end() ) 
	{
		std::cout << "level does not exist" << std::endl;
		return;
	}

	currentLevel = levelList[levelName.c_str()];
}

void LevelManager::update()
{
	currentLevel->update();
}


