#include "TileManager.h"
#include "RenderManager.h"


TileManager * TileManager::instance = nullptr;

TileManager::TileManager()
{

}

void TileManager::createInstance()
{
	if( instance ) return;

	instance = new TileManager();
}

void TileManager::deleteInstance()
{
	if( !instance ) return;

	delete instance;
	instance = nullptr;
}

TileManager* TileManager::getInstance()
{
	if( !instance ) createInstance();
	return instance;
}


void TileManager::draw(const Tile * tile)
{

	if( tile->getType() == GRASS )
		RenderManager::getInstance()->renderRenderable("GrassTile");
	else if(tile->getType() == WATER)
		RenderManager::getInstance()->renderRenderable("WaterTile");
	else if(tile->getType() == DIRT)
		RenderManager::getInstance()->renderRenderable("DirtTile");
}




