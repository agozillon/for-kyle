#ifndef RADIO_BUTTON_LIST_H
#define RADIO_BUTTON_LIST_H
#include "CheckBox.h"



class RadioButtonList: public Widget
{
public:
	RadioButtonList();



	
protected:
	unsigned int maxSelectableButtons; //The maximum number of buttons that can be selected
	bool deleteQueueWidgets();

};


#endif