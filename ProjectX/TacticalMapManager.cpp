#include "TacticalMapManager.h"

#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>


#include "ShaderManager.h"
#include "CameraManager.h"
#include "RenderManager.h"
#include "TextureManager.h"
#include "GameManager.h"
#include "Scenery.h"
#include "Mouse.h"

#include <SDL.h>
#include <stdio.h>

TacticalMapManager* TacticalMapManager::instance = NULL;

void TacticalMapManager::createInstance()
{
	if( instance ) return;

	instance = new TacticalMapManager();
}

void TacticalMapManager::deleteInstance()
{

	if( !instance) return;

	delete instance;
	instance = NULL;
}

TacticalMapManager* TacticalMapManager::getInstance()
{
	if( !instance ) createInstance();

	return instance;
}

void TacticalMapManager::changeTacticalMode(TacticalModeIdentifier id)
{
	if(id == DEPLOYMENT_MODE)
		currentTacticalMode = deploymentMode;
	
	if(id == COMBAT_MODE)
		currentTacticalMode = combatMode;
}

TacticalMapManager::TacticalMapManager()
{
	CameraManager::getInstance()->addCamera("TacticalMap", vec3(0.0f, 0.0f, 0.0),vec3(0.0f, 0.0f, -1.0f),vec3(0.0f, 1.0f, 0.0f), 60.0f, 1.0f, 400.0f);
	CameraManager::getInstance()->setActiveCamera("TacticalMap");
	CameraManager::getInstance()->rotateCamera(0.0f, 30.0f);

	ShaderManager::getInstance()->use("simpleTac");//just the same as usual glUse(program)

	deploymentMode = new TacticalDeployment();
	combatMode = new TacticalCombat();
	currentTacticalMode = deploymentMode; 

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(GL_TRUE);
}

TacticalMapManager::~TacticalMapManager()
{
	delete deploymentMode;
	delete combatMode;
}

void TacticalMapManager::draw()
{
	currentTacticalMode->draw();
}

void TacticalMapManager::update()
{
	currentTacticalMode->update();
}

void TacticalMapManager::checkKeyPress()
{
	currentTacticalMode->interaction();	
}