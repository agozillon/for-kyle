#ifndef MAP_H
#define MAP_H
#include <vector>
#include "Tile.h"


class Map
{
public:
	Map(std::vector<std::vector<Tile>>* tiles);

	void draw();

	int getHeight();
	int getWidth();


//	Tile & getTile(float x,float y); // get the tile at position

	void setDimentions(int w,int h);

	//this expects the x and y to be in screenspace coordinates
	void checkMouseCollision();

	const std::vector<std::vector<Tile>>* getMap();
private:
	std::vector<std::vector<Tile>> *map;
	int width,height;
};

#endif