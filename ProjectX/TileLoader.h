#ifndef TILE_LOADER_H
#define TILE_LOADER_H
#include <string>
#include <vector>
#include "Map.h"

class TileLoader
{
public:
	static Map* load(const std::string filePath);
};

#endif