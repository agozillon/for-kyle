#ifndef TEXTURE_LOADER_H
#define TEXTURE_LOADER_H
#include "Texture.h"
#include <string>
#include <iostream>

enum ImageType
{
	BMP,
	PNG
};

class TextureLoader
{
public:
	virtual Texture load(const std::string filePath) = 0;
	
	static TextureLoader * getLoader(ImageType i);
};


// additional loaders in here
#include "BMPLoader.h"

#endif