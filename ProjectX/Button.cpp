#include "Button.h"
#include "RenderManager.h"
#include "GameManager.h"


Button::Button(const std::string &name,const float ix,const float iy,const float w,const float h,unsigned int  f ): Widget(name,ix,iy,w,h,f),illuminationMode(BUTTON_INVISIBLE)
{

}


Button::Button(const std::string &name,const Sprite * sprite, float ix,const float iy,unsigned int f,const unsigned int  i)
	:Widget(name,ix,iy,
	(float)TextureManager::getInstance()->getWidthScreensapce(sprite->getTexture()), //convert width into screenspace coords
	(float)TextureManager::getInstance()->getHeightScreensapce(sprite->getTexture())//convert height into screenspace coords
	,f),
	sprite(sprite->getName())
{
	spriteDown = "NULL"; 
	illuminationMode = (i == BUTTON_SPRITE_SWAP ? BUTTON_SHADER_TINT : i);
}

	
Button::Button(const std::string &name,const Sprite *sprite,const Sprite * spriteDown,float ix,const float iy,unsigned int  f,const unsigned int  i)
	:Widget(name,ix,iy,
	(float)TextureManager::getInstance()->getWidthScreensapce(sprite->getTexture()), //convert width into screenspace coords
	(float)TextureManager::getInstance()->getHeightScreensapce(sprite->getTexture())//convert height into screenspace coords
	,f),
	illuminationMode(i),
	sprite(sprite->getName()),
	spriteDown(spriteDown->getName())
{}


void Button::draw() const
{
	if( (illuminationMode & BUTTON_ILLUMINATION_MODE_RESERVED) == BUTTON_INVISIBLE ) //if the button is set to be invisable
		return;

	glm::mat4 mvp(1.0f);

	mvp = glm::translate(mvp,glm::vec3(x,y,0));
	mvp = glm::scale(mvp,glm::vec3(width,height,0));


	glDepthMask(GL_FALSE);

	ShaderManager::getInstance()->use("GUI");
	ShaderManager::getInstance()->getShader("GUI")->setUniformMatrix4fv("MVP",1,false,glm::value_ptr(mvp));
	ShaderManager::getInstance()->getShader("GUI")->setUniform1f("tint",1.0f);



	StatusFlags interactionRequired = (illuminationMode & BUTTON_ONCLICK) ? STATUS_LEFT_CLICKED : STATUS_MOUSE_OVER;


	if( illuminationMode & BUTTON_SPRITE_SWAP )
	{
		if( status & interactionRequired || (illuminationMode & BUTTON_ALWAYS) )RenderManager::getInstance()->renderRenderable(spriteDown); // if the mouse is over swap the renderable to be drawn
		else RenderManager::getInstance()->renderRenderable(sprite);
	}else if( illuminationMode & BUTTON_SHADER_TINT ) {
		if( !(status & interactionRequired) && !(illuminationMode & BUTTON_ALWAYS) ) ShaderManager::getInstance()->getShader("GUI")->setUniform1f("tint",0.75f); //make the button slightly darker when the mouse isn't over it
		RenderManager::getInstance()->renderRenderable(sprite);
	} 
	else RenderManager::getInstance()->renderRenderable(sprite);


	glDepthMask(GL_TRUE);

}