#ifndef PLAYER_MANAGER_H
#define PLAYER_MANAGER_H
#include <vector>
#include <time.h>
#include "Unit.h"
#include "Pathfinder.h"
#include "DamageEnvironmentalEffect.h"
#include "SlowEnvironmentalEffect.h"

class PlayerManager {
public:
	PlayerManager(std::vector<Unit*> playerUnits);
	~PlayerManager();
	void draw(glm::mat4 viewProjection);
	void update(std::vector<Scenery*> scene, std::vector<Unit*> enemyUnits, std::vector<DamageEnvironmentalEffect*> dmgEffect, std::vector<SlowEnvironmentalEffect*> slowEffect);
	void interaction(std::vector<Scenery*> scene, std::vector<Unit*> enemyUnits);
	void getUnits(std::vector<Unit*> &u){u = units;}
	void updatePlayerSelected(Unit* playerSelection){playerSelected = playerSelection;}

private:
	std::vector<Unit*> units;
	Unit * playerSelected;

	PathFinder * pathSystem;
	clock_t previousTime;
	clock_t timePassed;
};
#endif
