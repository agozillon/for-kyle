#ifndef TEXT_H
#define TEXT_H
#include "Widget.h"
#include "Sprite.h" // doesn't actually use this to draw, just needs some data from it during construction
#include "TextToTexture.h"

//Basic Text class used to display text as a widget on the screen
class Text: public Widget
{
public:
	//Create an invible button
	Text(const std::string &name, const std::string textureRenderTargetName,  const std::string fontName, const std::string displayText, const int fontSize, Uint8 r, Uint8 g, Uint8 b, float ix,const float iy, float w, float h, unsigned int f);
	~Text();
	void draw() const;
	virtual bool checkMouse() { return Widget::checkMouse(); }
	void updateText(std::string);

protected:
	TextToTexture * text;
	std::string sprite;
	SDL_Color colour;
};

#endif