#include "TacticalDeployment.h"
#include "Button.h"
#include "Text.h"
#include "WindowWidget.h"
#include "TacticalDeploymentWidgetEventHandler.h"
#include "CameraManager.h"
#include "Mouse.h"
#include "GameManager.h"
#include <ctime>
#include <stdlib.h>


TacticalDeployment::TacticalDeployment()
{
	levelManager = new LevelManager();

	// these hardcoded weaknesses and stats should only be temporary, original plan is/was
	// to get the stats passed into the unit manager via text then send here
	std::vector<std::string> musketeerWeakness, musketeerStrength, cannonWeakness, cannonStrength, lancerWeakness, lancerStrength;
	musketeerWeakness.push_back("Cannon"); musketeerStrength.push_back("Lancer");   
	cannonWeakness.push_back("Lancer"); cannonStrength.push_back("Musketeer");
	lancerWeakness.push_back("Musketeer"); lancerStrength.push_back("Cannon");
	
	
	enemyUnits.push_back(new Unit("Cannon", cannonWeakness, cannonStrength, vec3(0.0f, 0.0f, -4.8f), vec3(0.0f, 0.0f,  0.0f), vec3(0.5f, 0.5f, 0.5f), vec3(2.0f, 2.0f, 2.0f), 1, 100, 3, 0.1, 4, 2, "Log", "Fabric",  "simpleTac"));
	enemyUnits.push_back(new Unit("Musketeer", musketeerWeakness, musketeerStrength, vec3(0.0f, 0.0f, -4.8), vec3(0.0f, 0.0f,  0.0f), vec3(0.5f, 0.5f, 0.5f), vec3(2.0f, 2.0f, 2.0f), 1, 25, 5, 0.1, 25, 5, "Log", "Fabric",  "simpleTac"));
	enemyUnits.push_back(new Unit("Lancer", lancerWeakness, lancerStrength, vec3(0.0f, 0.0f, -4.8f), vec3(0.0f, 0.0f, 0.0f), vec3(0.5f, 0.5f, 0.5f), vec3(2.0f, 2.0f, 2.0f), 1, 100, 3, 0.1, 10, 3, "Log", "Fabric",  "simpleTac"));


	playerUnits.push_back(new Unit("Musketeer", musketeerWeakness, musketeerStrength, vec3(0.0f, 0.0f, -4.8f), vec3(0.0f, 0.0f, 0.0f), vec3(0.1f, 0.1f, 0.1f), vec3(2.0f, 2.0f, 2.0f), 1, 100, 3, 0.1, 25, 5, "Cube", "Fabric",  "simpleTac"));
	playerUnits.push_back(new Unit("Lancer", lancerWeakness, lancerStrength, vec3(0.0f, 0.0f, -4.8f), vec3(0.0f, 0.0f, 0.0f), vec3(0.1f, 0.1f, 0.1f), vec3(2.0f, 2.0f, 2.0f), 1, 100, 3, 0.1, 10, 3, "Cube", "Fabric",  "simpleTac"));
	playerUnits.push_back(new Unit("Cannon", cannonWeakness, cannonStrength, vec3(0.0f, 0.0f, -4.8f), vec3(0.0f, 0.0f, 0.0f), vec3(0.1f, 0.1f, 0.1f), vec3(2.0f, 2.0f, 2.0f), 1, 100, 3, 0.1, 4, 2, "Cube", "Fabric",  "simpleTac"));
	playerUnits.push_back(new Unit("Musketeer", musketeerWeakness, musketeerStrength, vec3(-0.0f, 0.0f, -4.8f), vec3(0.0f, 0.0f, 0.0f), vec3(0.1f, 0.1f, 0.1f), vec3(2.0f, 2.0f, 2.0f), 1, 100, 3, 0.1, 25, 5, "Cube", "Fabric",  "simpleTac"));
	playerUnits.push_back(new Unit("Musketeer", musketeerWeakness, musketeerStrength, vec3(-0.0f, 0.0f, -4.8f), vec3(0.0f, 0.0f, 0.0f), vec3(0.1f, 0.1f, 0.1f), vec3(2.0f, 2.0f, 2.0f), 1, 100, 3, 0.1, 25, 5, "Cube", "Fabric",  "simpleTac"));
	playerUnits.push_back(new Unit("Cannon", cannonWeakness, cannonStrength, vec3(0.0f, 0.0f, -4.8f), vec3(0.0f, 0.0f, 0.0f), vec3(0.1f, 0.1f, 0.1f), vec3(2.0f, 2.0f, 2.0f), 1, 100, 3, 0.1, 4, 2, "Cube", "Fabric",  "simpleTac"));
	
	initialDeployment(*levelManager->getPlayerDeploymentZone(), playerUnits);
	initialDeployment(*levelManager->getEnemyDeploymentZone(), enemyUnits);

	WindowWidget * window = new WindowWidget("Window",(const Sprite*)RenderManager::getInstance()->getRenderable("Windowbackdrop"),-0.5f, 0.73f, WIDGET_PERFORM_CHILD_CLEANUP, WINDOW_WIDGET_NULL );
	window->scale(3.0f,0.7f);

	TacticalDeploymentWidgetEventHandler * handler = new TacticalDeploymentWidgetEventHandler(levelManager, &playerUnits, &enemyUnits);
	widgetManager.addEventHandler(handler);
	
	Button *button = new Button("AcceptButton",(const Sprite*)RenderManager::getInstance()->getRenderable("AcceptButton"),-0.1f,0.75f,WIDGET_NULL,BUTTON_SHADER_TINT);
	button->scale(1.0f, 1.0f);	
	button->addEventHandler(widgetManager.getEventHandler());
	window->addWidget(button);

	// really shouldn't be a button its just text however theres no option for it, yet
	button = new Button("DeploymentText", (const Sprite*)RenderManager::getInstance()->getRenderable("DeploymentText"), -0.35f, 0.81f, WIDGET_NULL,BUTTON_NO_ILLUMINATION);
	button->scale(1.2f, 1.2f);
	window->addWidget(button);

	widgetManager.addWidget(window);	
}

// unintelligent deployment positioning, basically just randomizes and makes sure units don't get put in a Unit or piece of Scenery, they 
// shouldn't be put outside of the current levels deployment zone but they can accidently be pushed outside a bit if the units stack up on the
// edge and get seperated. 
void TacticalDeployment::initialDeployment(BoundingBox deploymentZone, std::vector<Unit*> units)
{
	srand(time(0));
	
	for(size_t i = 0; i < units.size(); ++i)
	{
		// basic randomizer to place the units within the deployment zone in a random formation -1.5 is to make it less likely the units are half in and half out the deployment zone
		glm::vec3 tmpPosition(deploymentZone.getPosition().x + (rand() / ((float)RAND_MAX / ((deploymentZone.getDimensions().x-1.5) * 2)) - deploymentZone.getDimensions().x)
			,deploymentZone.getPosition().y + (rand() / ((float)RAND_MAX / ((deploymentZone.getDimensions().y-1.5) * 2)) - deploymentZone.getDimensions().y),-4.8);

		units[i]->getCollisionBox()->updatePosition(tmpPosition);
		
		std::vector<Scenery*> tmpScene;
		levelManager->getScenery(tmpScene);
		
		for(size_t i2 = 0; i2 < tmpScene.size(); i2++)
		{
			if(units[i]->getCollisionBox()->detectBoxCollision(tmpScene[i2]->getCollisionBox()) && tmpScene[i2]->getUnitCollideable())
			{
				glm::vec3 moveDist = tmpScene[i2]->getCollisionBox()->seperationDistance(units[i]->getCollisionBox());
				tmpPosition = glm::vec3(tmpPosition.x + moveDist.x, tmpPosition.y + moveDist.y, tmpPosition.z);
			}
		}
		
		for(size_t i2 = 0; i2 < units.size(); i2++)
		{
			if(units[i]->getCollisionBox()->detectBoxCollision(units[i2]->getCollisionBox()) && i != i2)
			{
				glm::vec3 moveDist = units[i2]->getCollisionBox()->seperationDistance(units[i]->getCollisionBox());
				tmpPosition = glm::vec3(tmpPosition.x + moveDist.x, tmpPosition.y + moveDist.y, tmpPosition.z);
			}
		}
		
		units[i]->updatePosition(tmpPosition);
	}
}

void TacticalDeployment::draw()
{
	glm::mat4 projection = CameraManager::getInstance()->getProjection(); // usual GLM stuff
	glm::mat4 view = CameraManager::getInstance()->getLookAt();
	glm::mat4 viewProjection = projection * view;
	
	levelManager->draw(viewProjection);

	for(size_t i = 0; i < playerUnits.size(); ++i)
		playerUnits[i]->draw(viewProjection);

	for(size_t i = 0; i < enemyUnits.size(); ++i)
		enemyUnits[i]->draw(viewProjection);

	// don't have a specific texture to make something see-through, however Tree 3 seems to manage it
	TextureManager::getInstance()->bind("StartArea"); 
	levelManager->getPlayerDeploymentZone()->draw(viewProjection);

	widgetManager.draw();
}

void TacticalDeployment::update()
{
}

void TacticalDeployment::interaction()
{
	Uint8 * keys = SDL_GetKeyboardState(NULL);
	// move the camera to the left/right/up/down if a WASD key is pressed or the mouse is
	// at the edge of the screen. Each of the mouse at edge of screen checks have a different 
	// offset because using the WASD keys to move the screen actually jumps the mouse co-ordinates
	// to invalid values which coincidently go below the threshold in some cases making the screen
	// continue to scroll until the mouse is moved again. The various offsets stop this whilst keeping
	// the scrolling sensitive.
	if( keys[SDL_SCANCODE_W] || Mouse::getInstance()->getY() >= GameManager::getInstance()->getHeight() - 10)  
	{
		CameraManager::getInstance()->moveCameraUp(0.1f);
	}

	if( keys[SDL_SCANCODE_S] || Mouse::getInstance()->getY() <= 10)
	{
		CameraManager::getInstance()->moveCameraUp(-0.1f);
	}

	if( keys[SDL_SCANCODE_A] || Mouse::getInstance()->getX() <=  4 ) 
	{
		CameraManager::getInstance()->moveCameraRight(-0.1f);
	}

	if( keys[SDL_SCANCODE_D] || Mouse::getInstance()->getX() >= GameManager::getInstance()->getWidth() - 5)
	{
		CameraManager::getInstance()->moveCameraRight(0.1f); 
	}

	if( keys[SDL_SCANCODE_Z] )
	{
		CameraManager::getInstance()->moveCameraForward(0.1f);
	}

	if( keys[SDL_SCANCODE_X])
	{
		CameraManager::getInstance()->moveCameraForward(-0.1f);
	}

	// if it hits the GUI don't cast a ray (don't want to be able to click on things hidden)
	// it also checks if the GUI is being clicekd etc.
	if(!widgetManager.checkMouse())
	{
		// check collisions with the mouse to any object then pass in the position of the 
		// intersection to the unit to traverse to it
		if(Mouse::getInstance()->getLeftState() == BUTTON_STATE_DOWN)
		{
			Ray pickingRay = Mouse::getInstance()->calculateRay();
			bool rayHasCollided = false; 
		
			glm::vec3 temp;
			// player army unit click selection, break out once we've hit a target (wastes processing time continuing it when we've 
			// found the unit we wish to select)
			for(size_t i = 0; i < playerUnits.size(); i++)
			{
				if(playerUnits[i]->getCollisionBox()->detectRayCollision(pickingRay.rayOrigin, pickingRay.rayDirection, temp)) 
				{
					playerSelected.clear();
					playerSelected.push_back(playerUnits[i]);
					rayHasCollided = true;
					break;
				}
			}

		
			// essentially just means if a collision has already happened skip this check as it takes
			// priority (I.E a unit is infront/above the ground so the player intended to click the unit not the ground)
			// or if a unit hasn't been selected
			if(rayHasCollided == false && !playerSelected.empty())
			{
				std::vector<Scenery*> tmp;
				levelManager->getScenery(tmp);
				
				if(levelManager->getPlayerDeploymentZone()->detectRayCollision(pickingRay.rayOrigin, pickingRay.rayDirection, glm::vec3(0,0,0)))
				{
					for(size_t i = 0; i < tmp.size(); i++)
					{
						// loops through the whole scene and only detects collisions with the floor, gets around the issue of clicking on clusters(tree above a floor at an angle
						//ray should hit behind the tree but doesn't) of things and it not pathing to them, currently do this by checking if its collided and then also if the scenery is pickable
						if(tmp[i]->getCollisionBox()->detectRayCollision(pickingRay.rayOrigin, pickingRay.rayDirection, temp) 
							&& tmp[i]->getRayPickable() == true)
						{
							bool inObject = false;
							playerSelected[0]->getCollisionBox()->updatePosition(temp);

							for(size_t i2 = 0; i2 < tmp.size(); i2++)
							{
								if(playerSelected[0]->getCollisionBox()->detectBoxCollision(tmp[i2]->getCollisionBox()) && tmp[i2]->getUnitCollideable())
									inObject = true;
							}

							if(inObject == false)
								playerSelected[0]->updatePosition(temp);

							break;
						}
					}
				}
			}
		}
	}
}