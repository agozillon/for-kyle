// Fragment Shader � file "particle.frag"

#version 330

precision highp float; // needed only for version 1.30

out vec4 out_Color;

uniform sampler2D textureUnit0;

void main(void)
{
	out_Color = texture2D(textureUnit0, gl_PointCoord);
	
}
