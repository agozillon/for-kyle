#ifndef FACTION_MANAGER_H
#define FACTION_MANAGER_H
#include <vector>
#include "Faction.h"




class FactionManager
{
public:
	static FactionManager * getInstance();
	static void createInstance();
	static void deleteInstance();

	void add(Faction*faction);
	Faction*getFaction(const std::string &str) const;
	Faction*getFaction(const unsigned int index) const;

	const glm::vec4 & getColour(const std::string &str) const;
	const glm::vec4 & getColour(const unsigned int index) const;
private:
	FactionManager();
	~FactionManager();
	std::vector<Faction*> factions;

	static FactionManager * instance;

};

#endif