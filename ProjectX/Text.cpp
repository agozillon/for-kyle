#include "Text.h"
#include "RenderManager.h"
#include "GameManager.h"


Text::Text(const std::string &name, const std::string textureRenderTargetName, const std::string fontName, std::string displayText, const int fontSize, Uint8 r, Uint8 g, Uint8 b, float ix,const float iy, float w, float h, unsigned int f)
	: Widget(name,ix,iy,w,h,f)
{
	text = new TextToTexture(fontName, fontSize, textureRenderTargetName);
	colour.r = r; colour.g = g; colour.b = b;
	text->createTextTexture(displayText.c_str(), colour); 
	RenderManager::getInstance()->addRenderable(textureRenderTargetName, new Sprite(textureRenderTargetName));
	sprite = textureRenderTargetName;
}

void Text::updateText(std::string newText){
	text->createTextTexture(newText.c_str(), colour);
}

void Text::draw() const
{
	glm::mat4 mvp(1.0f);

	mvp = glm::translate(mvp,glm::vec3(x,y,0));
	mvp = glm::scale(mvp,glm::vec3(width,height,0));


	glDepthMask(GL_FALSE);

	ShaderManager::getInstance()->use("GUI");
	ShaderManager::getInstance()->getShader("GUI")->setUniformMatrix4fv("MVP",1,false,glm::value_ptr(mvp));
	ShaderManager::getInstance()->getShader("GUI")->setUniform1f("tint",1.0f);
	RenderManager::getInstance()->renderRenderable(sprite);


	glDepthMask(GL_TRUE);

}

Text::~Text()
{
	delete text;
}