#include "TacticalCombatWidgetEventHandler.h"
#include "TacticalMapManager.h"

void TacticalCombatWidgetEventHandler::handleMouseLeftClick(Widget *widget)
{

	if(strncmp(widget->getName().c_str(), "Card", 4) == 0 && widget->getPreviousTimeStamp() + 50 < widget->getTimeStamp() ){
		std::string tmpString;
		for(size_t i = 4; i < widget->getName().size(); i++)
			tmpString += widget->getName()[i];
		
		int i = atoi(tmpString.c_str());

		playerManager->updatePlayerSelected(playerUnits[i]);
	}

}
