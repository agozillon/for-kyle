#ifndef UNIT_H
#define UNIT_H
#include "WorldObjects.h"
#include "BoundingBox.h"
#include "Scenery.h"
#include <string>
using namespace glm;	

class Unit : public WorldObjects
{
public:
	const inline vec3 getPosition(){return position;}
	const inline vec3 getRotation(){return rotation;}
	const inline vec3 getScalar(){return scalar;}
	const inline int getHealth(){return health;}
	const inline void updatePath(std::vector<vec3> nPath){currentPath = nPath;}
	void draw(mat4 viewProjection);
	inline void updatePosition(vec3 pos){position = pos; rangeBox->updatePosition(pos); collisionBox->updatePosition(pos);}
	inline void updateRotation(vec3 rot){rotation = rot;}
	inline void updateScalar(vec3 scale){scalar = scale;}
	inline void updateHealth(int hp){health = hp; perUnitHealth = health / numberOfTroops;}
	inline void updateSlowCoefficent(float slowValue){slowCoefficient = slowValue;}
	inline void setTarget(Unit * nTarget){target = nTarget;} 
	void traversePath(std::vector<Scenery*> obstacles);
	inline BoundingBox * getCollisionBox(){return collisionBox;}
	inline BoundingBox * getRangeBox(){return rangeBox;}
	inline Unit * getTarget(){return target;}
	inline std::string getUnitType(){return unitType;}
	void calculateCombat(Unit* attacker);
	void damageUnit(int hp);
	bool operator > (const Unit * param);
	bool operator < (const Unit * param);
	bool operator == (const Unit * param);
	~Unit();
	Unit(std::string type, std::vector<std::string> weaknesses, std::vector<std::string> strengths, vec3 pos, vec3 rot, vec3 scale, 
	vec3 rng, int arm, int hp, int dmg, float sp, int unitCount, int unitsInRow, std::string mesh, std::string texture, std::string shader);
	
protected:
	std::vector<std::string> unitWeaknesses;
	std::vector<std::string> unitStrengths;
	std::string unitType;

private:
	// private function that calculates the collision box for the current unit, this needs to be recalculated every time
	// a few units die or else it'll skew 
	void calculateCollisionBox();

	vec3 scalar;
	vec3 rotation;
	vec3 position;
	vec3 range;
	vec3 soldiersDimensions;
	
	int armor;
	int health;
	int perUnitHealth;
	int damage;
	float speed;

	int numberOfTroops;
	int unitsPerRow;
	int rowCount;
	float slowCoefficient;
	
	BoundingBox * rangeBox;
	BoundingBox * collisionBox;
	
	Unit * target;
	
	std::vector<vec3> currentPath;
	
	std::string meshName;
	std::string textureName;
	std::string shaderName;
	
};

#endif