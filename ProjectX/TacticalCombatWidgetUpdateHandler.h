#ifndef TACTICAL_COMBAT_WIDGET_UPDATE_HANDLER_H
#define TACTICAL_COMBAT_WIDGET_UPDATE_HANDLER_H
#include "WidgetUpdateHandler.h"
#include "Widget.h"
#include "Unit.h"
#include "LevelManager.h"

class TacticalCombatWidgetUpdateHandler: public WidgetUpdateHandler
{
public: 
	TacticalCombatWidgetUpdateHandler(std::vector<Unit*> pUnits); 
	void update(Widget *);

private:
	std::vector<Unit*> playerUnits;
	std::vector<int> previousHealth;
};

#endif