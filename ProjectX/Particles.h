#ifndef PARTICLES_H
#define PARTICLES_H
#include <GL\glew.h>
#include <glm/gtc/type_ptr.hpp> // including glm/gtc/type_ptr so that this class can use vec3s 
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;
#include <string>

class Particles {
public:
	Particles(const int numOfParticles, const vec3 emitterDimensions, const vec3 emitterPosition, const vec2 screenResolution, const float particleSize, std::string texture, std::string shader);
	~Particles();
	int getNumParticles() const {return numParticles;}
	void update();
	void draw(mat4 view, mat4 projection);
	

private:
	void init();
	int numParticles;
	float particleSize;
	std::string textureName;
	std::string shaderName;
	glm::vec3 centerPosition;
	glm::vec3 dimensions;
	GLfloat* positions;
	GLfloat* velocity;
	vec2 screenSize;
	GLuint vaoID;
	GLuint vboID;
};

#endif
