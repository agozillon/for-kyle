#include "Unit.h"
#include "RenderManager.h"
#include <stack>
#include <cmath>


Unit::Unit(std::string type, std::vector<std::string> weaknesses, std::vector<std::string> strengths, vec3 pos, vec3 rot, vec3 scale, 
	vec3 rng, int arm, int hp, int dmg, float sp, int unitCount, int unitsInRow, std::string mesh, std::string texture, std::string shader)
	: unitType(type), unitWeaknesses(weaknesses), unitStrengths(strengths), position(pos), rotation(rot), scalar(scale), range(rng), armor(arm), health(hp), damage(dmg), speed(sp), 
	numberOfTroops(unitCount), unitsPerRow(unitsInRow), meshName(mesh), textureName(texture), shaderName(shader), target(NULL), slowCoefficient(1.0)
{
	
	// divide by unitsPerRow and round up to get our row count (used a fair amount so more optimal to store it and recalculate it on change)
	rowCount = ceil((float)numberOfTroops / unitsPerRow);

    perUnitHealth = health / numberOfTroops;

	// setting up range box
	rangeBox = new BoundingBox(position, range);
		
	// creating AABB, grabbing vertex count and passing in to create temporary boundingBox for a single unit
	// so we can then calculate the larger BoundingBox
	int count = RenderManager::getInstance()->getRenderableToMesh(meshName.c_str())->getVertexCount();
	float * tmp = new float[count];
	RenderManager::getInstance()->getRenderableToMesh(meshName.c_str())->getVertData(tmp);
	BoundingBox temp = BoundingBox(position, tmp, count, scalar);
	delete tmp;

	soldiersDimensions = temp.getDimensions();
	
	// instantiate collision box and then resize it using the Unit classes function.
	collisionBox = new BoundingBox(position, glm::vec3(0,0,0));
	calculateCollisionBox();
}

// not the same as updateHealth which is for flatly changing the units hp, instead 
// this is for damaging the unit externally(I.E not combat calculations) for things
// like environmental effects
void Unit::damageUnit(int hp)
{
	health = hp;

	numberOfTroops = ceil((float)health / perUnitHealth);
	rowCount = ceil((float)numberOfTroops / unitsPerRow);
	calculateCollisionBox();
}

// may seem like a convuluted way to draw things but to position them well enough for a single
// bounding box it's required. However I'm unsure if it'd be more beneficial to draw them basically
// spaced apart and then check for collisions for every single soldier in the unit instead of one overarching box
// would be more accurate for detection. May also be a good idea to pre-calculate the small amount of maths
// required to see if things are even and calculate it every time a unit dies. I know small optimiziation but...
void Unit::draw(mat4 viewProjection)
{
	std::stack<mat4> mStack;
	bool columnEven = false;
	bool rowEven = false; 
	vec3 positions;
	float farLeft;
	
	if(rowCount % 2 == 0)
		rowEven = true;

	if(unitsPerRow % 2 == 0)
		columnEven = true;

	int horizontal = unitsPerRow / 2; 
	int vertical = rowCount / 2;
	
	// applying rotations to the whole unit rather than individuals
	mStack.push(mat4(1.0));
	mStack.top() = translate(mStack.top(), position);
	mStack.top() = rotate(mStack.top(), rotation.x, vec3(1, 0, 0));
	mStack.top() = rotate(mStack.top(), rotation.y, vec3(0, 1, 0));
	mStack.top() = rotate(mStack.top(), rotation.z, vec3(0, 0, 1));
	
	if(columnEven == false)
	{
		// moving position x to the farthest away model on the left
		for(int i = 0; i < horizontal; i++)
			positions.x -= soldiersDimensions.x * 4;

	}
	else
	{
		positions.x = -soldiersDimensions.x * 2;
		
		// moving position x to the farthest away model on the left
		for(int i = 0; i < horizontal-1; i++)
			positions.x -= soldiersDimensions.x * 4;
	}

	farLeft = positions.x; // we move left to right so we save the far left to go back to it
	
	if(rowEven == true)
	{
		positions.y = soldiersDimensions.y * 2; // moving position into the center of a model
		
		// moving position y to top most row
		for(int i = 0; i < vertical-1; i++)
			positions.y += soldiersDimensions.y * 4;
	}
	else
	{
		// moving position y to top most row
		for(int i = 0; i < vertical; i++)
			positions.y += soldiersDimensions.y * 4;
	}
	
	for(int i = 0; i < numberOfTroops; i++)
	{
		// now traverse left then right, before moving up a row till upValues are reached
		
		// i != 0 as i modulo 0 would always equal 0 irregardless of divisor
		if(i % unitsPerRow == 0 && i != 0)
		{
			positions.x = farLeft;
			positions.y -= soldiersDimensions.y * 4; 
		}
		
		
		// draw object
		mStack.push(mStack.top());
		mStack.top() = translate(mStack.top(), positions);
		mStack.top() = scale(mStack.top(), scalar);
		
		// combining the viewProjection(Projection * view) and the model matrix to make the MVP(projection * view * model) 
		mat4 temp = viewProjection * mStack.top();
		ShaderManager::getInstance()->use(shaderName);
		ShaderManager::getInstance()->getShader(shaderName.c_str())->setUniformMatrix4fv("MVP", 1, false, value_ptr(temp));  //set the MVP
		TextureManager::getInstance()->bind(textureName.c_str()); // Find and bind that texture
		RenderManager::getInstance()->renderRenderable(meshName.c_str()); // render that renderable on the screen
		mStack.pop();

		positions.x += soldiersDimensions.x * 4;
	}

	/*
	TEST
	// below for test and display purposes only, shows the position of the group as a whole
	// and thus where the bbox originates from and the group rotates around.
	
	mStack.push(mat4(1.0));
	mStack.top() = translate(mStack.top(), position);
	mStack.top() = scale(mStack.top(), scalar);
	mStack.top() = rotate(mStack.top(), rotation.x, vec3(1, 0, 0));
	mStack.top() = rotate(mStack.top(), rotation.y, vec3(0, 1, 0));
	mStack.top() = rotate(mStack.top(), rotation.z, vec3(0, 0, 1));

	mat4 temp = viewProjection * mStack.top();
	ShaderManager::getInstance()->getShader(shaderName.c_str())->setUniformMatrix4fv("MVP", 1, false, value_ptr(temp));  //set the MVP
	TextureManager::getInstance()->bind(textureName.c_str()); // Find and bind that texture
	RenderManager::getInstance()->renderRenderable(meshName.c_str()); // render that renderable on the screen
	mStack.pop();
	*/
}

// works with a previously instantiated (inside the constructor) collision box that represents the unit as a whole
// and resizes it based on the current units size and shape 
void Unit::calculateCollisionBox()
{
	glm::vec3 newDimensions;
	
	newDimensions.x = ((unitsPerRow * soldiersDimensions.y * 2) + ((unitsPerRow - 1) * soldiersDimensions.y * 2)) / 2;	
	newDimensions.y = ((rowCount * soldiersDimensions.y * 2) + ((rowCount - 1) * soldiersDimensions.y * 2)) / 2;
	newDimensions.z = soldiersDimensions.z;
	
	collisionBox->updateDimensions(newDimensions);
}

// very rudimentary version of this function for demo purposes, basically increments using speed
// to a point in the current path list(directly) it currently doesn't loop through a list it only checks the first
// since we're only passing in a single value for an intersection on an object for test purposes. The
// + 0.1 and - 0.1 on the checks are to stop float precision errors causing the object to jitter as it 
// never exactly reaches the point we're aiming for!
void Unit::traversePath(std::vector<Scenery*> obstacles)
{
	
	for(size_t i = 0; i < obstacles.size(); i++)
	{
		if(obstacles[i]->getCollisionBox()->detectBoxCollision(collisionBox) && obstacles[i]->getUnitCollideable() == true)
		{
			collisionBox->updatePosition(position);
			// Unit is collider, obstacle is collidee we now want the seperation distance to split the objects apart
			// this is only ever a problem when an Unit is following another Unit (can occassionally get stuck within another object)
			glm::vec3 moveDist = obstacles[i]->getCollisionBox()->seperationDistance(collisionBox);
			position = glm::vec3(position.x + moveDist.x, position.y + moveDist.y, position.z); 
			collisionBox->updatePosition(position);
		}
	}

	bool inRange = false;
	// essentially assumes that if there is no target then it can't be in range, so doesn't check
	// for a collision (avoiding calling a non-existant pointer for the most part) 
	if(target != NULL)
	     inRange = rangeBox->detectBoxCollision(target->getCollisionBox());

	// just checks if the current path list is empty, if it isn't it moves the unit slightly closer
	// to the next node based on if its less than or greater than it. Once it's near it within a set bias
	// it removes the node from the list and updates the collision box
	if(!currentPath.empty() && !inRange)
	{
		vec3 tmpPosition = currentPath[currentPath.size()-1];

		if(position.x < tmpPosition.x - 0.1f)
			position.x += speed * slowCoefficient; 
		else if(position.x > tmpPosition.x + 0.1f)
			position.x -= speed * slowCoefficient;

		if(position.y < tmpPosition.y - 0.1f)
			position.y += speed * slowCoefficient; 
		else if(position.y > tmpPosition.y + 0.1f)
			position.y -= speed * slowCoefficient;
	
		if(position.y <= tmpPosition.y + 0.1f && position.y >= tmpPosition.y - 0.1f
			&& position.x <= tmpPosition.x + 0.1f && position.x >= tmpPosition.x - 0.1f) 
		{
			currentPath.erase(currentPath.end()-1);
		}

		collisionBox->updatePosition(position);
		rangeBox->updatePosition(position);
	}
}


// function that calculates the damage done to the unit using the function based off of the passed in units
// various statistics.
void Unit::calculateCombat(Unit* attacker)
{
	int modifiedDamage = attacker->damage;

	// if this units health is greater or equal to 0 and targetting this unit then
	// calculate damage done
	if(health >= 0 && attacker->getTarget() == this)
	{
		// 1 = bonus damage due to the attacker being this units weakness
		for(size_t i = 0; i < this->unitWeaknesses.size(); i++)
			if(attacker->unitType == this->unitWeaknesses[i])
				modifiedDamage = attacker->damage + 1;

		// -1 = mitigating damage due to the attacker being weak against this unit
		for(size_t i = 0; i < this->unitStrengths.size(); i++)
			if(attacker->unitType == this->unitStrengths[i]) 
				modifiedDamage = attacker->damage - 1;

		health -= modifiedDamage;
	
		numberOfTroops = ceil((float)health / perUnitHealth);
		rowCount = ceil((float)numberOfTroops / unitsPerRow);
		calculateCollisionBox();
	}
	
}

// basic operator overloads so that we can tell if a unit is >(stronger), <(weaker) or ==(equal) to another unit 
bool Unit::operator > (const Unit * param)
{
	for(size_t i = 0; i < this->unitStrengths.size(); i++)
		if(this->unitStrengths[i] == param->unitType)
			return true;

	return false;
}

bool Unit::operator < (const Unit * param)
{
	for(size_t i = 0; i < this->unitWeaknesses.size(); i++)
		if(this->unitWeaknesses[i] == param->unitType)
			return true;

	return false;
}

bool Unit::operator == (const Unit * param)
{
	if(this->unitType == param->unitType)
		return true;
	else
		return false;
}
	
Unit::~Unit()
{
	delete collisionBox;
	delete rangeBox;
}