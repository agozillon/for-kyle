#ifndef TACTICAL_COMBAT_H
#define TACTICAL_COMBAT_H
#include "TacticalMode.h"
#include "WidgetManager.h"

class TacticalCombat : public TacticalMode
{
public:
	~TacticalCombat();
	TacticalCombat();
	void draw();
	void update();
	void interaction();
	void setMap(LevelManager * lManager, std::vector<Unit*> pUnits, std::vector<Unit*> eUnits);

private:
	WidgetManager widgetManager;
	LevelManager * levelManager;
	EnemyManager * enemyManager;
	PlayerManager * playerManager;
};
#endif