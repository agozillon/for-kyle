#include "AudioManager.h"
#include <iostream>

AudioManager* AudioManager::instance = NULL;

AudioManager::AudioManager()
{
	/* Initialize default output device */
	if (!BASS_Init(-1, 44100, 0, 0, NULL))
	{
		std::cout << "Can't initialize device or device already Initialized" << std::endl;
	}
}
void AudioManager::createInstance()
{
	if( instance ) return;
	instance = new AudioManager();
}

void AudioManager::deleteInstance()
{
	if( !instance ) return;
	delete instance;
	instance = NULL;
}

AudioManager * AudioManager::getInstance()
{
	if( !instance ) createInstance();
	return instance;
}

void AudioManager::load(std::string audioName, std::string fileName)
{
	audioFiles.insert(std::pair<std::string, Audio*>(audioName, new Audio(fileName.c_str())));
}

void AudioManager::playAudio(std::string audioName)
{
	audioFiles[audioName]->play();
}

void AudioManager::resumeAudio(std::string audioName)
{
	audioFiles[audioName]->resume();
}

void AudioManager::pauseAudio(std::string audioName)
{
	audioFiles[audioName]->pause();
}

void AudioManager::updateVolume(std::string audioName, int volume)
{
	audioFiles[audioName]->updateVolume(volume);
}

AudioManager::~AudioManager()
{
	for (std::map<std::string, Audio*>::iterator it= audioFiles.begin(); it!=audioFiles.end(); ++it)
		delete it->second;

	audioFiles.clear();
}