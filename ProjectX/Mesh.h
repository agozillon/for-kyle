#ifndef MESH_H
#define MESH_H
#include "Renderable.h"


/*
	Just a blank mesh
	
*/
class Mesh: public Renderable
{
public:

	//You pass into this only the attributes you want to use, the rest will be left unassigned
	Mesh(float *verts,const unsigned int count,float *uv = NULL,const unsigned int uvCount= 0,
		unsigned short *indices = NULL,const unsigned int indiceCount = 0, float * normals = NULL,const unsigned int normalCount = 0);
	~Mesh();

	void render() const;

	// data required for creating BoundingBoxes
	void getVertData(float * data) const {for(unsigned int i = 0; i < count; i++){data[i] = vertData[i];}}
	const unsigned int getVertexCount()const {return count;}

private:
	GLuint VAO;
	GLuint vertexVBO,indiceVBO,uvVBO,normalVBO;
	const unsigned int count;
	const unsigned int indiceCount;
	
	// for easy creation of bounding boxes
	float * vertData;

	void indexDraw() const;
	void draw() const;

	// set to false if they are not initally passed into the constructor, true if they are
	bool indices,uvs,normals;

};

#endif