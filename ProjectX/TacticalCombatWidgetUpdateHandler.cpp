#include "TacticalCombatWidgetUpdateHandler.h"
#include "Text.h"
#include <sstream>

TacticalCombatWidgetUpdateHandler::TacticalCombatWidgetUpdateHandler(std::vector<Unit*> pUnits){
	playerUnits = pUnits; 

	for(size_t i = 0; i < playerUnits.size(); i++)
		previousHealth.push_back(playerUnits[i]->getHealth());
}

void TacticalCombatWidgetUpdateHandler::update(Widget *widget)
{
	Text * tmp;
	std::ostringstream s;

	if(strncmp(widget->getName().c_str(), "HealthText", 10) == 0){
		std::string tmpString;
		for(size_t i = 10; i < widget->getName().size(); i++)
			tmpString += widget->getName()[i];

		tmp = (Text*)widget;
		int i = atoi(tmpString.c_str());
		
		// basically a check to see if we need to do the rest. Recreating a texture every x seconds
		// for 6 things is incredibly CPU heavy.
		if(previousHealth[i] != playerUnits[i]->getHealth()){
			if(playerUnits[i]->getHealth() <= 0)			
				s << "Dead ";  // stream data	
			else
				s << "Health " << playerUnits[i]->getHealth();  // stream data

			tmpString = "";
			tmpString = s.str();
			tmp->updateText(tmpString);
			previousHealth[i] = playerUnits[i]->getHealth();
		}
	}
}

