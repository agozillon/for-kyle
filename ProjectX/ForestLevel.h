#ifndef FOREST_LEVEL_H
#define FOREST_LEVEL_H
#include "Scenery.h"
#include "Level.h"
#include "DamageEnvironmentalEffect.h"
#include "SlowEnvironmentalEffect.h"
#include <vector>

class ForestLevel : public Level
{
public:
	ForestLevel();
	~ForestLevel();
	void init();
	void draw(glm::mat4 viewProjection);
	void update();
	inline void getScenery(std::vector<Scenery*> &scenery){scenery = sceneryList;}
	inline BoundingBox * getPlayerDeploymentZone(){return playerDeploymentZone;}
	inline BoundingBox * getEnemyDeploymentZone(){return enemyDeploymentZone;}
	inline void getDamageEnvironmentalEffects(std::vector<DamageEnvironmentalEffect*> &damageEffects){damageEffects = damageEffectList;}
	inline void getSlowEnvironmentalEffects(std::vector<SlowEnvironmentalEffect*> &slowEffects){slowEffects = slowEffectList;}
	
private:
	std::vector<Scenery*> sceneryList;
	std::vector<DamageEnvironmentalEffect*> damageEffectList;
	std::vector<SlowEnvironmentalEffect*> slowEffectList;
	BoundingBox * playerDeploymentZone;
	BoundingBox * enemyDeploymentZone;
};
#endif