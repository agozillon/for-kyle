#ifndef RANDOM_MAP_GENERATOR_H
#define RANDOM_MAP_GENERATOR_H
#include "Map.h"
#include <vector>

typedef std::pair<int,int> TILE_XY;


enum MapType
{
	SMALL_ISLANDS,
	LARGE_ISLANDS,
	CONTINENTS,
	MEDITERRANEAN,
	LANDMASS
};

class MapGenerator
{
public:
	MapGenerator() {} 
	~MapGenerator() {} 
	Map * generateMap(const int width,const int height,const int numberOfFactions,MapType);
private:
	std::vector<float> generateHeightMap(const  int width,const  int height);
    
	enum Direction
	{
		UP,
		DOWN,
		RIGHT,
		LEFT,
		UPPER_RIGHT,
		UPPER_LEFT,
		BOTTOM_RIGHT,
		BOTTOM_LEFT,
		CENTER //for the seed tile
	};

	void spawnTilesAround(const int x,const int y,const int elevation,Direction directionFrom);
	void setFactionFromPoint(const int x,const int y,const  int distance,const  int factionNumber,Direction directionFrom);

	std::vector<std::pair<TILE_XY,int>> getSeedTiles(MapType);	//Semi-randomly select a number of points to be the mountain seeds


	std::vector<std::vector<Tile>> *tiles;

	std::vector<TILE_XY> landTiles;    //Land provinces 
	std::vector<TILE_XY> factionTiles; //Tiles occupied by a faction (only takes center tile) 
	int width,height;

	int factionDomainSize;
	int seaLevel; // in range 0-1000

	void removeLoners();

};

#endif