#ifndef ENEMY_MANAGER_H
#define ENEMY_MANAGER_H
#include <vector>
#include <time.h>
#include "Unit.h"
#include "Pathfinder.h"
#include "DamageEnvironmentalEffect.h"
#include "SlowEnvironmentalEffect.h"

class EnemyManager {
public:
	EnemyManager(std::vector<Unit*> enemyUnits);
	~EnemyManager();
	void draw(glm::mat4 viewProjection);
	void update(std::vector<Scenery*> scene, std::vector<Unit*> playerUnits, std::vector<DamageEnvironmentalEffect*> dmgEffect, std::vector<SlowEnvironmentalEffect*> slowEffect);
	void targetSelection(std::vector<Unit*> playerUnits, Unit * currentUnit);
	void getUnits(std::vector<Unit*> &u){u = units;}

private:
	std::vector<Unit*> units;
	PathFinder * pathSystem;
	bool targetTrigger;
	clock_t previousTime;
	clock_t timePassed;
};
#endif

