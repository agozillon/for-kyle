#include "SlowEnvironmentalEffect.h"

SlowEnvironmentalEffect::SlowEnvironmentalEffect(float slowAmount, const vec3 pos, const vec3 dimensions, const std::string texture, const std::string shader, const int particleCount, const glm::vec2 screenResolution)
	: slowCoefficient(slowAmount)
{
	indicator = new Particles(particleCount, dimensions, pos, screenResolution, 0.1f, texture, shader);
	collisionBox = new BoundingBox(pos, dimensions);
}

void SlowEnvironmentalEffect::IncurEnviromentalEffect(Unit * effectedUnit)
{
	effectedUnit->updateSlowCoefficent(slowCoefficient);
}

// basic draw method that calls the particles class draw method, it also updates the particles!
// I decided it would be better to encapsulate away the particles needs for updating rather than
// creating another function call specifically for it.
void SlowEnvironmentalEffect::draw(mat4 view, mat4 projection)
{
	indicator->update();
	indicator->draw(view, projection);
}

SlowEnvironmentalEffect::~SlowEnvironmentalEffect()
{
	delete indicator;
	delete collisionBox;
}
