#include "TileLoader.h"
#include <fstream>
#include <sstream>
#include "Tile.h"
#include <iostream>

Map* TileLoader::load(const std::string filePath)
{
	std::vector<std::vector<Tile>> * vec = new std::vector<std::vector<Tile>>();

	std::ifstream file(filePath,std::ios::ate | std::ios::in | std::ios::binary);

	int size = file.tellg();

	file.seekg(file.beg);
	char * block = new char[size];
	file.read(block,size);
	file.close();

	std::stringstream stream;
	stream << block;
	delete[] block;


	int width,height;
	stream >> width >> height;

	const unsigned int TILE_TYPE = 0xFF,RESOURCE_TYPE = 0xFF00, RESOURCE_AMOUNT = 0xFF0000,OWNER = 0XFF000000; // declare masks for extracting the padded data

	
	vec->resize(width);
	for(int i = 0; i < vec->size(); i++)
	{
		(*vec)[i].resize(height);
	}


	unsigned int w =0,h =height-1;
	while( h < height && stream.good() ) //unsigned overflow will cause h to become 4*10^31
	{
		int temp;
		stream >> std::hex >> temp;
		unsigned char type = temp&TILE_TYPE;
		unsigned char rType = (temp&RESOURCE_TYPE)>>8;
		unsigned char rAmount = (temp&RESOURCE_AMOUNT)>>16;
		unsigned char owner = (temp&OWNER)>>24;


		(*vec)[w][h] =  Tile(type,rType,rAmount,owner);
		w++;
		if( w == width )
		{
			w = 0;
			h--;
		}
		

	}

	Map *map = new Map(vec);

	map->setDimentions(width,height);
	return map;
}