#ifndef STRATEGIC_MODE_WIDGET_EVENT_HANDLER_H
#define STRATEGIC_MODE_WIDGET_EVENT_HANDLER_H
#include "WidgetEventHandler.h"
#include "Widget.h"

class StrategicModeWidgetEventHandler: public WidgetEventHandler
{
public: 
	StrategicModeWidgetEventHandler() { }
	void handleMouseLeftClick(Widget *);
};

#endif