#ifndef GAME_MANAGER_H
#define GAME_MANAGER_H

#include "GameState.h"


enum GameStateIdentifier
{
	MAIN_MENU,
	STRATEGIC_MAP,
	TACTICAL_MAP,

};



class GameManager
{
public:

	static void createInstance(const unsigned int w,const unsigned int h);
	static void deleteInstance();
	static GameManager*getInstance();

	void setDimentions(const unsigned int w,const unsigned int h); // sets the dimentions of the SDL Window 
	void draw();
	void update();
	void checkState();


	void changeState(GameStateIdentifier newState);

	~GameManager();


	const unsigned int getWidth()  { return SCREEN_WIDTH; }
	const unsigned int getHeight() { return SCREEN_HEIGHT;}

private:
	GameStateIdentifier stateId,previousState;

	const unsigned int SCREEN_WIDTH,SCREEN_HEIGHT;
	
	void initData();

	GameState *state;

	static GameManager * instance;

	GameManager(const unsigned int w,const unsigned int h);
	GameManager(); // So this class can only be constructed and deleted through its own static functions
};

#endif