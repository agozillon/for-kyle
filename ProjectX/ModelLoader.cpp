#include "ModelLoader.h"
#include "ObjLoader.h"
#include "AssimpLoader.h"


ModelLoader * ModelLoader::getLoader(ModelFormat mf)
{
	if( mf == OBJ ) 
		return new ObjectLoader();

	if(mf == ASS )
		return new AssimpLoader();

	std::cout << "Error unknown model format" << std::endl;

	return NULL;
}