#include "PlayerManager.h"
#include "Mouse.h"

PlayerManager::PlayerManager(std::vector<Unit*> playerUnits)
{
	pathSystem = new PathFinder();
	pathSystem->updateStepDistance(0.5f);
	units = playerUnits;
	playerSelected = NULL;
	timePassed = 0;
	previousTime = clock();
}

PlayerManager::~PlayerManager()
{
	delete pathSystem;

	for(size_t i = 0; i < units.size(); i++)
		delete units[i];
}

void PlayerManager::update(std::vector<Scenery*> scene, std::vector<Unit*> enemyUnits, std::vector<DamageEnvironmentalEffect*> dmgEffect, std::vector<SlowEnvironmentalEffect*> slowEffect)
{	
	timePassed += clock() - previousTime;

	// we only really want to update these things every 3/4 a second
	if(timePassed > 750)
	{
		// combat between units
		for(size_t i = 0; i < enemyUnits.size(); i++)
			for(size_t i2 = 0; i2 < units.size(); i2++)
				if(enemyUnits[i]->getRangeBox()->detectBoxCollision(units[i2]->getCollisionBox()))
					units[i2]->calculateCombat(enemyUnits[i]);
				
	
		// damage effects due to environmental effects
		for(size_t i = 0; i < units.size(); i++)	
			for(size_t i2 = 0; i2 < dmgEffect.size(); i2++)	
				if(units[i]->getCollisionBox()->detectBoxCollision(dmgEffect[i2]->getCollisionBox()))
					dmgEffect[i2]->IncurEnviromentalEffect(units[i]);

		// slow environmental effects
		for(size_t i = 0; i < units.size(); i++)	
			for(size_t i2 = 0; i2 < slowEffect.size(); i2++)	
				if(units[i]->getCollisionBox()->detectBoxCollision(slowEffect[i2]->getCollisionBox()))
					slowEffect[i2]->IncurEnviromentalEffect(units[i]);
				else
					units[i]->updateSlowCoefficent(1.0f); // resets the slow coefficent back to 1.0

		// pathing check 
		for(size_t i = 0; i < units.size(); i++)
		{
			if(units[i]->getTarget() != NULL && units[i]->getHealth() >= 0)
			{
				pathSystem->setTarget(units[i], units[i]->getTarget()->getPosition());
				pathSystem->path(scene);
				std::vector<glm::vec3> tmpPath;
				pathSystem->getPath(tmpPath);
				units[i]->updatePath(tmpPath);
			}
		}

		timePassed = 0; // put time passed back to 0
	}
	
	previousTime = clock();

	// traverse the path given by the pathfinder as long as the units not dead
	for(size_t i = 0; i < units.size(); i++)
		if(units[i]->getHealth() >= 0)
			units[i]->traversePath(scene);
	
}

void PlayerManager::interaction(std::vector<Scenery*> scene, std::vector<Unit*> enemyUnits)
{
		// clear the players current selection
	if(Mouse::getInstance()->getRightState() == BUTTON_STATE_DOWN)
		playerSelected = NULL;

	// check collisions with the mouse to any object then pass in the position of the 
	// intersection to the unit to traverse to it
	if(Mouse::getInstance()->getLeftState() == BUTTON_STATE_DOWN)
	{
		Ray pickingRay = Mouse::getInstance()->calculateRay();
		bool rayHasCollided = false; 

		glm::vec3 temp;
		// player army unit click selection, break out once we've hit a target (wastes processing time continuing it when we've 
		// found the unit we wish to select)
		for(size_t i = 0; i < units.size(); i++)
		{
			if(units[i]->getCollisionBox()->detectRayCollision(pickingRay.rayOrigin, pickingRay.rayDirection, temp)) 
			{
				playerSelected = NULL;
				playerSelected = units[i];
				rayHasCollided = true;
				break;
			}
		}

		
		if(rayHasCollided == false && playerSelected != NULL)
		{
			for(size_t i = 0; i < enemyUnits.size(); i++)
			{
				if(enemyUnits[i]->getCollisionBox()->detectRayCollision(pickingRay.rayOrigin, pickingRay.rayDirection, temp)) 
				{
					playerSelected->setTarget(enemyUnits[i]);
					rayHasCollided = true;
					break;
				}
			}
		}
		
		// essentially just means if a collision has already happened skip this check as it takes
		// priority (I.E a unit is infront/above the ground so the player intended to click the unit not the ground)
		// or if a unit hasn't been selected
		if(rayHasCollided == false && playerSelected != NULL)
		{
			for(size_t i = 0; i < scene.size(); i++)
			{
				// loops through the whole scene and only detects collisions with the floor, gets around the issue of clicking on clusters(tree above a floor at an angle
				//ray should hit behind the tree but doesn't) of things and it not pathing to them, currently do this by checking if its collided and then also if the scenery is pickable
				if(scene[i]->getCollisionBox()->detectRayCollision(pickingRay.rayOrigin, pickingRay.rayDirection, temp) 
					&& scene[i]->getRayPickable() == true)
				{
					pathSystem->setTarget(playerSelected, temp);
					pathSystem->path(scene);
					std::vector<glm::vec3> tmpPath;
					pathSystem->getPath(tmpPath);
					playerSelected->updatePath(tmpPath);
					playerSelected->setTarget(NULL);
					break;
				}
			}
		}
	}

}

void PlayerManager::draw(glm::mat4 viewProjection)
{
	for(size_t i = 0; i < units.size(); i++){
		units[i]->draw(viewProjection);
		units[i]->getCollisionBox()->draw(viewProjection);
	}
}